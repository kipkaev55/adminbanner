<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation for table `viewer`.
 */
class m160821_142936_create_viewer_table extends Migration
{
  public function up()
  {
      $this->createTable('viewer', [
          'id' => $this->primaryKey(),
          'client' => $this->text(),
          'ipAddress' => $this->string(15),
          'bannerId' => $this->smallInteger()->unsigned(),
          'countView' => $this->integer(),
          'countClick' => $this->integer(),
          'createdAt' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
      ]);
  }

  public function down()
  {
      $this->dropTable('viewer');
  }
}
