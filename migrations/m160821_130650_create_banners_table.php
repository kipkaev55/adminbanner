<?php

use yii\db\Migration;

/**
 * Handles the creation for table `banners`.
 */
class m160821_130650_create_banners_table extends Migration
{
  public function up()
  {
      $this->createTable('banners', [
          'id' => $this->primaryKey(),
          'value' => $this->text(),
          'urlImage' => $this->string(255),
          'urlLink' => $this->string(255),
      ]);
  }

  public function down()
  {
      $this->dropTable('banners');
  }
}
