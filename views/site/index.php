<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


$this->title = 'My Yii Application';
$options = [
    'template' => '{label}{input}{error}',
    'labelOptions' => ['class' => 'col-sm-2 control-label']
];
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p><?php
             if ($model) {
               echo '<div id="myelement" data-link="'.(($model->urlLink) ? $model->urlLink : '').'" >';
               if ($model->value){
                 echo $model->value;
               } elseif ($model->urlImage) {
                 echo '<img style="max-width:736px" src="/uploads/banners/'.$model->urlImage.'">';
               } else {
                 echo '<div><p>SALE</p><p>736x93</p></div>';
               }
               echo '</div>';
             }
            ?></p>
    </div>

</div>
<pre>
<?=($err) ? $err : ''?>
</pre>
<script>
function sendData(type){
  var http = new XMLHttpRequest();
  var url = "/create";
  var typeValue = 'view';
  var params = "client=<?=\Yii::$app->request->userAgent?>&ipAddress=<?=\Yii::$app->request->userIP?>&banner=<?=($model) ? $model->id : ''?>&type=" + type;
  http.open("POST", url, true);

  //Send the proper header information along with the request
  http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  http.onreadystatechange = function() {//Call a function when the state changes.
    if(http.readyState == 4 && http.status == 200) {
        // console.log(http.responseText);
    }
  }
  http.send(params);
}
</script>
