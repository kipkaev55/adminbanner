<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Banner;
use app\models\Viewer;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout','my-method'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'my-method' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    private $models;
    public function actionIndex()
    {
      $this->models = Banner::find()->joinWith([
        'viewer v' => function ($query) {
          $today = new \DateTime();
          $query
          // ->andWhere(['between', 'createdAt', "2016-08-21", "2016-08-23" ])
          // ->andWhere(['between', 'createdAt', "2016-08-21", "2016-08-23" ])//, 'between', 'createdAt' => ['2016-08-21', '2016-08-23']
          ->onCondition(['v.ipAddress' => \Yii::$app->request->userIP,'v.client' => \Yii::$app->request->userAgent])
          ->andOnCondition(['between', 'createdAt', $today->format('Y-m-d'), $today->modify('+1 day')->format('Y-m-d') ])
          ->orOnCondition(['v.ipAddress' => 'NULL','v.client' => 'NULL']);
    },
      ])
      ->select(['banners.id','value','urlImage', 'urlLink', 'order','v.countView','v.createdAt'])
      // ->where('ipAddress = "' . \Yii::$app->request->userIP .'" OR ipAddress = NULL')
      ->groupBy(['banners.id','value','urlImage', 'urlLink', 'order','v.countView','v.createdAt'])
      ->orderBy('v.countView ASC')
      // ->orderBy('v.createdAt DESC')
      ->One();
      //->where('ipAddress = "' . \Yii::$app->request->userIP .'"')
      // Banner::find()->joinWith([
      //   'viewer',
      // ],null,'RIGHT JOIN')->One();
      //
        // $this->models = Banner::find()->orderBy([
        //   'order' => SORT_ASC
        // ])->One();
        // $banners = Banner::find()->joinWith([
        //   'viewer v',
        // ])->where('ipAddress = "' . \Yii::$app->request->userIP .'"')->orderBy('v.countView ASC')->All();
        $err='';
        if($this->models === null) {
          $this->models = Banner::find()->One();
          $err='empty';
        }
      return $this->render('index', ['model' => $this->models, 'err' => $err]);
    }

    public function actionMyMethod()
    {
        $post = Yii::$app->request->post();
        $model = $this->findViewer($post);
        if($post['type']=='click'){
          if ($model !== null) {
              $model->countClick = $model->countClick + 1;
              $model->save();
              return 'click';
          }
        }
        if ($model !== null) {
            $model->countView = $model->countView + 1;
            $model->save();
            return 'ok';
        } else {
            $viewer = new Viewer();
            $viewer->client = $post['client'];
            $viewer->ipAddress = $post['ipAddress'];
            $viewer->bannerId = $post['banner'];
            $viewer->countView = 1;
            $viewer->save();
            return 'create';
        }
    }

    public function beforeAction($action)
    {
      if ($this->action->id == 'my-method') {
          Yii::$app->controller->enableCsrfValidation = false;
      }
      return parent::beforeAction($action);
    }

    protected function findModel($id)
    {
        if (($model = Banner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findViewer($post)
    {
        $today = new \DateTime();
        $model = Viewer::find()
        ->where(['between', 'createdAt', $today->format('Y-m-d'), $today->modify('+1 day')->format('Y-m-d') ])
        ->andWhere([
            'client' => $post['client'],
            'ipAddress' => $post['ipAddress'],
            'bannerId' => $post['banner'],
        ])
        ->One()
        ;
        return $model;
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
