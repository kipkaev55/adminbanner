<?php
if(YII_DEBUG) {
    return [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=192.168.99.100;port=33061;dbname=banner',
        'username' => 'banner',
        'password' => 'banner',
        'charset' => 'utf8',
    ];
}
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=127.0.0.1;dbname=banner',
    'username' => 'banner',
    'password' => 'banner',
    'charset' => 'utf8',
];
