<?php

return [
    '/' => 'site/index',
    'admin/' => 'admin/default/login',
    'admin' => 'admin/default/login',
    'create' => 'site/my-method',
];
