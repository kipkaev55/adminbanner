<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Viewer;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;

class StatController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Viewer::findBySql("SELECT  DATE(createdAt) as createdAt, COUNT(concat(client,ipAddress)) as client, bannerId, SUM(countView) as countView, SUM(countClick) as countClick FROM viewer GROUP BY DATE(createdAt), bannerId ORDER BY createdAt DESC"),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

}
