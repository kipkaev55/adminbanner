<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Viewer;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
// use yii\web\NotFoundHttpException;
// use yii\filters\VerbFilter;
// use vova07\fileapi\actions\UploadAction as FileAPIUpload;

class ViewerController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
        ];
    }

    // public function actions()
    // {
    //     return [
    //         'fileapi-upload' => [
    //             'class' => FileAPIUpload::className(),
    //             'path' => '@webroot/uploads/temp'
    //         ],
    //     ];
    // }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Viewer::find()
            ->orderBy([
              'createdAt' => SORT_DESC
            ]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Viewer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
