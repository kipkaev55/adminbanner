<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Просмотры баннеров';
$this->params['breadcrumbs'][] = $this->title;
?>

<h2 class="section-header"><?= Html::encode($this->title) ?> </h2>

<div class="panel panel-default panel-minimal">
    <div class="panel-body">
        <div class="row">
          <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => '{items}{pager}',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'client',
                    'ipAddress',
                    'bannerId',
                    'countView',
                    [
                      'attribute' => 'countClick',
                      'value' => function ($data) {
                        if (!$data->countClick) {
                            return 0;
                        } else {
                            return $data->countClick;
                        }
                      },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Действия',
                        'headerOptions' => ['width' => '80'],
                        'template' => '{delete}',
                    ],
                ],
                'tableOptions' => ['class' => 'table no-margin'],
            ]); ?>
        </div>
    </div>
</div>
