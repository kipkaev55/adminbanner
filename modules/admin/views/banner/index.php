<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Баннеры';
$this->params['breadcrumbs'][] = $this->title;
?>

<h2 class="section-header"><?= Html::encode($this->title) ?> <?= Html::a('<span class="fa fa-plus"></span> Добавить', ['create'], ['class' => 'btn btn-info btn-xs ui-wave']) ?></h2>

<div class="panel panel-default panel-minimal">
    <div class="panel-body">
        <div class="row">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => '{items}{pager}',
                'rowOptions' => function ($model, $key, $index, $grid) {
                    return ['data-sortable-id' => $model->id];
                },
                'columns' => [
                    [
                        'class' => \kotchuprik\sortable\grid\Column::className(),
                    ],
                    'id',
                    [
                      'attribute' => 'value',
                      'contentOptions' =>['style'=>'word-break: break-all;'],
                    ],
                    [
                        'label' => 'Изображение',
                        'format' => 'raw',
                        'value' => function ($data) {
                            if (!$data->urlImage) {
                                return '';
                            }

                            return Html::img('/uploads/banners/' . $data->urlImage, [
                                'style' => 'max-width:100px',
                            ]);
                        },
                    ],
                    [
                      'attribute' => 'urlLink',
                      'contentOptions' =>['style'=>'word-break: break-all;'],
                      'content'=>function($data){
                          return Html::a($data->urlLink,$data->urlLink,['target'=>'_blank']);
                      }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Действия',
                        'headerOptions' => ['width' => '80'],
                        'buttonOptions' => ['class' => 'btn btn-link'],
                        'template' => '{update}  {delete}',
                    ],
                ],
                'options' => [
                    'data' => [
                        'sortable-widget' => 1,
                        'sortable-url' => \yii\helpers\Url::toRoute(['sorting']),
                    ]
                ],
                'tableOptions' => ['class' => 'table no-margin'],
            ]); ?>
        </div>
    </div>
</div>
