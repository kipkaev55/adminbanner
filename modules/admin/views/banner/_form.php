<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\fileapi\Widget as FileAPI;

/* @var $this yii\web\View */
/* @var $model app\models\Type */
/* @var $form yii\widgets\ActiveForm */

$options = [
    'template' => '{label}<div class="col-sm-10">{input}{error}</div>',
    'labelOptions' => ['class' => 'col-sm-2 control-label']
];
$behavior = $model->getBehavior('uploadBehavior');
?>

<div class="col-md-8 col-md-offset-2">
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
    ]); ?>


    <?= $form->field($model, 'value', $options)
          ->widget(conquer\codemirror\CodemirrorWidget::className(), [
              'preset' => 'php',
              'options' => [
                  'rows' => 4,
              ],
          ])
           ?>

    <?= $form->field($model, 'urlLink', $options)->textInput() ?>

    <?= $form->field($model, 'urlImage', $options)->widget(
        FileAPI::className(),
        [
            'settings' => [
                'url' => ['fileapi-upload'],
                'lang' => 'ru',
            ],
            'preview' => false,
        ]
    ); ?>

    <p class="text-center">
        <img style="max-width: 130px;" src="<?= $behavior->urlAttribute('urlImage') ?>">
    </p>


    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => 'btn-info md-raised md-primary btn-w-md md-button md-ink-ripple']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
