<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TYpe */

$this->title = 'Изменение баннера №: ' . $model->id;
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="panel panel-default">
    <div class="panel-body padding-xl">
        <div class="row">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
