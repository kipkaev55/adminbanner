<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статистика баннеров';
$this->params['breadcrumbs'][] = $this->title;
?>

<h2 class="section-header"><?= Html::encode($this->title) ?> </h2>

<div class="panel panel-default panel-minimal">
    <div class="panel-body">
        <div class="row">
          <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => '{items}{pager}',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'createdAt',
                    'bannerId',
                    'countView',
                    [
                        'attribute'=>'client',
                        'label'=>'Уникальных (агент/ip-адрес)',
                    ],
                    [
                      'attribute' => 'countClick',
                      'value' => function ($data) {
                        if (!$data->countClick) {
                            return 0;
                        } else {
                            return $data->countClick;
                        }
                      },
                    ],
                ],
                'tableOptions' => ['class' => 'table no-margin'],
            ]); ?>
        </div>
    </div>
</div>
