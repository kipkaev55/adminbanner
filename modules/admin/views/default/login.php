<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
  <div class="login-logo">
    <span><b>Admin</b>BANNER</span>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
  <p class="login-box-msg">Sign in to start your session</p>
  <?php $form = ActiveForm::begin([
      'id' => 'login-form',
      'options' => ['class' => ''],
  ]); ?>

      <?= $form->field($model, 'username', ['template' => '<div class="form-group has-feedback">{input}<span class="glyphicon glyphicon-envelope form-control-feedback"></span></div>'])
          ->textInput(['placeholder' => $model->getAttributeLabel('username'), 'options' => ['class' => 'form-control'],]) ?>

      <?= $form->field($model, 'password', ['template' => '<div class="form-group has-feedback">{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span></div>'])
          ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'options' => ['class' => 'form-control'],]) ?>
    <div class="row">
      <div class="col-xs-8">
        <div class="checkbox icheck">

        </div>
      </div>
      <!-- /.col -->
      <div class="col-xs-4">
        <?= Html::submitButton('ВОЙТИ', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
      </div>
      <!-- /.col -->
    </div>
  <?php ActiveForm::end(); ?>
</div>
<!-- /.login-box-body -->
