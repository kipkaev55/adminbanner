<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "viewer".
 *
 * @property integer $id
 * @property string $client
 * @property string $ipAddress
 * @property integer $bannerId
 * @property integer $countView
 * @property integer $countClick
 */
class Viewer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'viewer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client'], 'string'],
            [['bannerId', 'countView', 'countClick'], 'integer'],
            [['ipAddress'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client' => 'UserAgent',
            'ipAddress' => 'IP-адрес клиента',
            'bannerId' => 'ID баннера',
            'countView' => 'Количество просмотров',
            'countClick' => 'Количество кликов',
            'createdAt' => 'Дата',
        ];
    }
    public function getBanners()
    {
        return $this->hasOne(Banner::className(), ['id' => 'bannerId']);
    }
}
