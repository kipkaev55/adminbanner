<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use vova07\fileapi\behaviors\UploadBehavior;

/**
 * This is the model class for table "banners".
 *
 * @property integer $id
 * @property string $value
 * @property string $urlImage
 * @property string $urlLink
 */
class Banner extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'string'],
            [['urlImage', 'urlLink'], 'string', 'max' => 255],
        ];
    }

    public function behaviors()
        {
            return [
                'uploadBehavior' => [
                    'class' => UploadBehavior::className(),
                    'attributes' => [
                        'urlImage' => [
                            'path' => '@webroot/uploads/banners',
                            'tempPath' => '@webroot/uploads/temp',
                            'url' => '/uploads/banners'
                        ]
                    ]
                ],
                'sortable' => [
                    'class' => \kotchuprik\sortable\behaviors\Sortable::className(),
                    'query' => self::find(),
                ],
            ];
        }

    public function getUploadBehavior()
    {
        return $this->getBehavior('uploadBehavior');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'HTML-код',
            'urlImage' => 'Изображение',
            'urlLink' => 'Ссылка',
        ];
    }

    public function getViewer()
    {
        return $this->hasMany(Viewer::className(), ['bannerId' => 'id']);
    }
}
