<?php
namespace app\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte';

    public $css = [
        'bootstrap/css/bootstrap.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
        'dist/css/AdminLTE.css',
        'dist/css/skins/_all-skins.min.css',
        'plugins/iCheck/square/blue.css',
    ];

    public $js = [
      // 'plugins/jQuery/jquery-2.2.3.min.js',
      'bootstrap/js/bootstrap.min.js',
      'plugins/iCheck/icheck.min.js'
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
